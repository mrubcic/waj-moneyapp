insert into users (username, password, enabled)
values ('admin', '$2a$10$eG28hqAjihXGfSyrNUji9OZEGnMNh66uQUjjIBU0OaaE4Os4u1tom', 1);

insert into users (username, password, enabled)
values ('student', '$2a$10$XUil1gwD8eWVxsCl4T0WmuvWr/u/eOR/colwWalMWa.4rw.BK7Unm', 1);

insert into authorities (username, authority) values ('admin', 'ROLE_ADMIN');

insert into authorities (username, authority) values ('admin', 'ROLE_USER');

insert into authorities (username, authority) values ('student', 'ROLE_USER');



insert into wallets( name, wallet_type, username) values ( 'Novcanik', 'KOZNI', 'admin');

insert into wallets( name, wallet_type, username) values ( 'MojNovi', 'KOZNI', 'student');

insert into expenses( name, cost, cost_type, wallet_id) values ( 'Pizza', 25, 'FOOD', 1 );
insert into expenses( name, cost, cost_type, wallet_id) values ( 'Hamburger', 15, 'FOOD', 1 );
insert into expenses( name, cost, cost_type, wallet_id) values ( 'Sok', 13, 'DRINK', 1 );



