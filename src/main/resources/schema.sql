create table if not exists users (
   username varchar(20) not null primary key,
   password varchar(100) not null,
   enabled bit not null
);
create table if not exists authorities (
   username varchar(20) not null,
   authority varchar(20) not null,
   foreign key (username) references users(username)
);

create table if not exists wallets(
    id long auto_increment primary key,
    name varchar(255) not null,
    wallet_type varchar(255),
    create_date datetime,
    username varchar(255) not null,
    foreign key (username) references users(username)
);

create table if not exists expenses(
    id long auto_increment primary key,
    name varchar(255) not null,
    cost decimal not null,
    cost_type varchar(255),
    create_date datetime,
    wallet_id int(255) not null,
    foreign key (wallet_id) references wallets(id)
);


