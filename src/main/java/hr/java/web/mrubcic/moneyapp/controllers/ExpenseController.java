package hr.java.web.mrubcic.moneyapp.controllers;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.models.validation.ExpenseGroup;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/expense")
@SessionAttributes({"expenseTypes","wallets","wallet"})
@Slf4j
public class ExpenseController {

    private final WalletService walletService;
    private final ExpenseService expenseService;

    @Autowired
    public ExpenseController(final WalletService walletService,final ExpenseService expenseService) {
        this.walletService = walletService;
        this.expenseService = expenseService;
    }

    @GetMapping()
    public String showForm(final Model model, final Principal principal){

        model.addAttribute("expense", new Expense());
        model.addAttribute("expenseTypes", CostType.values());
        model.addAttribute("wallets", walletService.getUserWallets(principal.getName()));
        model.addAttribute("wallet",new Wallet());

        return "expenseForm";
    }

    @PostMapping()
    public String formResult(@Validated(value = ExpenseGroup.class) final Expense expense, final Errors expenseErrors, final Model model){

        if(expenseErrors.hasErrors()){

            log.info("Form inputs not complete");
            return "expenseForm";
        }

        final Expense persistedExpense = expenseService.save(expense);

        model.addAttribute("date", LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.YYYY")));
        model.addAttribute("expense", persistedExpense);
        model.addAttribute("wallet",persistedExpense.getWallet());

        return "formResponse";
    }

    @GetMapping("/resetWallet")
    public String resetWallet(final Wallet wallet){

        expenseService.deleteAllWhereWalletIDEquals(wallet.getId());

        return "redirect:/expense";
    }

    @PostMapping("/search")
    public String postSearch(final String keyword, final Model model){

        final List<Expense> searchedExpenses = expenseService.getExpensesStartingWith(keyword);

        model.addAttribute("searchedExpenses", searchedExpenses);

        return "searchExpenses";
    }

    @GetMapping("/search")
    public String showSearchScreen(final Model model){

        model.addAttribute("searchedExpenses", new ArrayList<>());
        model.addAttribute("keyword","");

        return "searchExpenses";
    }


}
