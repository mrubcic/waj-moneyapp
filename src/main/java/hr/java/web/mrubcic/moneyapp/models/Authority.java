package hr.java.web.mrubcic.moneyapp.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "AUTHORITIES")
public class Authority {

    public Authority(final String authority, final String username) {
        this.authority = authority;
        this.username = username;
    }

    @Id
    @Column(name = "USERNAME")
    private String username;

    @Column(name = "AUTHORITY")
    private String authority;

}
