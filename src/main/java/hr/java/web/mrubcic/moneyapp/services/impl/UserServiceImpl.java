package hr.java.web.mrubcic.moneyapp.services.impl;

import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.repositories.UserRepository;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import hr.java.web.mrubcic.moneyapp.services.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    public User createUser(final User user) {

        user.setEnabled(true);

        //encode password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        //set basic authorities
        user.setAuthorities(Collections.singletonList(new Authority("ROLE_USER", user.getUsername())));

       return userRepository.save(user);
    }

    public boolean isExistingWithUsername(final String username) {
        return userRepository.existsById(username);
    }


    public User getUser(final String username) {

        Optional<User> userOptional = userRepository.findById(username);

        if(userOptional.isEmpty()){

            throw new ResourceNotFoundException("There is no user with username " + username);
        }

        return userOptional.get();
    }


    public User updateUser(final User user) {
        return userRepository.save(user);
    }


    public void delete(final String username) {
        userRepository.deleteById(username);
    }
}
