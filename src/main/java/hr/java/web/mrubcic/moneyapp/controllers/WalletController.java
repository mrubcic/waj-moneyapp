package hr.java.web.mrubcic.moneyapp.controllers;

import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.security.Principal;

@Controller
@RequestMapping("/wallet")
@SessionAttributes("walletTypes")
public class WalletController {

    private final WalletService walletService;
    private final UserService userService;

    @Autowired
    public WalletController(WalletService walletService, UserService userService) {

        this.walletService = walletService;
        this.userService = userService;
    }

    @GetMapping("/create")
    public String showForm(final Model model){

        model.addAttribute("wallet",new Wallet());
        model.addAttribute("walletTypes", WalletType.values());

        return "walletForm";
    }

    @PostMapping("/create")
    public String formResponse(@Validated final Wallet wallet, final Errors errors, final Principal principal){

        if(errors.hasErrors()){

            return "walletForm";
        }

        wallet.getUser().setUsername(principal.getName());
        walletService.save(wallet);

        return "redirect:/expense";
    }
}
