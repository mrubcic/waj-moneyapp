package hr.java.web.mrubcic.moneyapp.quartz;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class ExpenseStatisticsJob extends QuartzJobBean {

    @Autowired
    private ExpenseService expenseService;

    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        final List<Expense> expenses = expenseService.getAll();

        final Map<CostType, Double> sumByCostType = expenses
                .stream()
                .collect(Collectors.groupingBy(Expense::getCostType,
                        Collectors.summingDouble(value -> value.getCost().doubleValue())));


        final Map<CostType, Double> maxByCostType = expenses.stream()
                .collect(Collectors.toMap(
                        Expense::getCostType,
                        value -> value.getCost().doubleValue(),
                        BinaryOperator.maxBy(Double::compareTo)
                ));


        final Map<CostType, Double> minByCostType = expenses.stream()
                .collect(Collectors.toMap(
                        Expense::getCostType,
                        value -> value.getCost().doubleValue(),
                        BinaryOperator.minBy(Double::compareTo)
                ));

        System.out.println("EXPENSE STATS: ");
        System.out.println("\t\t SUM\t\t MIN\t\tMAX");
        for (CostType costType : CostType.values()) {

            if(sumByCostType.get(costType) != null){

                System.out.println(costType.name + "\t" +
                        sumByCostType.get(costType) + "\t\t" +
                        minByCostType.get(costType) + "\t\t" +
                        maxByCostType.get(costType) + "\t\t");
            }
        }

        System.out.println("\n\n");
    }
}
