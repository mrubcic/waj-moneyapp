package hr.java.web.mrubcic.moneyapp.controllers.rest;

import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserRestController {

    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public User save(@RequestBody  final User user){
        return userService.createUser(user);
    }

    @PutMapping
    public User update(@RequestBody final User user){
        return userService.updateUser(user);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{username}")
    public void delete(@PathVariable("username") final String username){
        userService.delete(username);
    }

    @GetMapping("/{username}")
    public User get(@PathVariable("username") final String username){
        return userService.getUser(username);
    }

}
