package hr.java.web.mrubcic.moneyapp.services;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface ExpenseService {

    @Transactional
    Expense save(final Expense expense);

    List<Expense> getAllWhereWalletIdEquals(final long id);

    @Transactional
    void deleteAllWhereWalletIDEquals(final long id);

    List<Expense> getAll();

    Expense getById(final Long id);

    @Transactional
    Expense update(final Expense expense);

    @Transactional
    void deleteById(final Long id);

    List<Expense> getExpensesStartingWith(final String keyword);
}
