package hr.java.web.mrubcic.moneyapp.repositories;


import hr.java.web.mrubcic.moneyapp.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String> {

}
