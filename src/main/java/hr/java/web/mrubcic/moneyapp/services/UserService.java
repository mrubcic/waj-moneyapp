package hr.java.web.mrubcic.moneyapp.services;

import hr.java.web.mrubcic.moneyapp.models.User;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface UserService {

    @Transactional
    User createUser(final User user);

    boolean isExistingWithUsername(final String username);

    User getUser(final String username);

    @Transactional
    User updateUser(final User user);

    @Transactional
    void delete(final String username);
}
