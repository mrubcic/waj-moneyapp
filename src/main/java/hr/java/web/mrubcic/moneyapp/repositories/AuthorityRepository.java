package hr.java.web.mrubcic.moneyapp.repositories;

import hr.java.web.mrubcic.moneyapp.models.User;

public interface AuthorityRepository {

    User save(final User user);
}
