package hr.java.web.mrubcic.moneyapp.services.impl;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.repositories.ExpenseRepository;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;
    private final WalletService walletService;

    @Autowired
    public ExpenseServiceImpl(ExpenseRepository expenseRepository, WalletService walletService) {
        this.expenseRepository = expenseRepository;
        this.walletService = walletService;
    }

    public Expense save(final Expense expense) {

        final Expense persistedExpense = expenseRepository.save(expense);
        persistedExpense.setWallet(walletService.getById(expense.getWallet().getId()));

        return persistedExpense;
    }

    public List<Expense> getAllWhereWalletIdEquals(final long id) {
        return expenseRepository.findAllByWallet_Id(id);
    }

    public void deleteAllWhereWalletIDEquals(final long id) {
        expenseRepository.deleteAllByWallet_Id(id);
    }

    public List<Expense> getAll() {
        return (List<Expense>) expenseRepository.findAll();
    }

    public Expense getById(final Long id) {
        return expenseRepository.findById(id).get();
    }

    public Expense update(final Expense expense) {
        return expenseRepository.save(expense);
    }

    public void deleteById(final Long id) {
        expenseRepository.deleteById(id);
    }

    @Override
    public List<Expense> getExpensesStartingWith(final String keyword) {
        return expenseRepository.findAllByNameStartingWithIgnoreCase(keyword);
    }
}
