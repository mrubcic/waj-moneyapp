package hr.java.web.mrubcic.moneyapp.models.enums;



public enum CostType {

    FOOD("Food"),
    DRINK("Drink"),
    PARTY("Party");

    public String name;

    CostType(String name) {
        this.name = name;
    }

}
