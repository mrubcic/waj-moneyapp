package hr.java.web.mrubcic.moneyapp.controllers.rest;

import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/wallet")
@CrossOrigin
public class WalletRestController {

    private final WalletService walletService;
    
    @Autowired
    public WalletRestController(final WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping
    public List<Wallet> findAll() {
        return walletService.getAll();
    }

    @GetMapping("/{id}")
    public Wallet findOne(@PathVariable final Long id) {
        return walletService.getById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Wallet saveOrUpdate(@RequestBody final Wallet wallet) {
        return walletService.save(wallet);
    }

    @PutMapping("/{id}")
    public Wallet update(@RequestBody final Wallet wallet) {
        return walletService.update(wallet);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        walletService.deleteById(id);
    }

    

}
