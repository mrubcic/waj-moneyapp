package hr.java.web.mrubcic.moneyapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "USERS")
public class User {

    @Id
    @Column(name = "USERNAME")
    @NotEmpty(message = "Username is required!")
    @Size(min = 2, max = 50, message = "Username should be between 2 and 50 characters long.")
    private String username;

    @Column(name = "PASSWORD")
    @NotEmpty(message = "Password is required!")
    @Size(min = 5, message = "Password should be atleast 5 characters long.")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USERNAME")
    @JsonIgnore
    private List<Authority> authorities;

    @JsonIgnore
    @Column(name = "ENABLED")
    private Boolean enabled;
}
