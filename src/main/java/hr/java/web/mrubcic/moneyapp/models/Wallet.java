package hr.java.web.mrubcic.moneyapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.models.validation.ExpenseGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.List;

@Data
@ToString(exclude = "expenses")
@EqualsAndHashCode(exclude = "expenses")
@Entity
@Table(name = "WALLETS")
public final class Wallet {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Positive(message = "{wallet.error.id}", groups = ExpenseGroup.class)
    private Long id;

    @Column(name = "NAME")
    @NotEmpty(message = "{wallet.error.name.empty}")
    @Length(min = 3,max = 50,message = "{wallet.error.name.length}")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "WALLET_TYPE")
    @NotNull(message = "{wallet.error.type}")
    private WalletType walletType;

    @OneToMany(mappedBy = "wallet", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Expense> expenses;

    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user = new User();


    public BigDecimal walletState(){
        return expenses.stream().map(Expense::getCost).reduce(BigDecimal.ZERO, BigDecimal::add).negate();
    }
}
