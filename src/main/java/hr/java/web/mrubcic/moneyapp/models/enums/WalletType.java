package hr.java.web.mrubcic.moneyapp.models.enums;

public enum WalletType {

    KOZNI("Kožni"),
    PLASTIKA("Plastični"),
    GUMA("Guma");

    public String name;

    WalletType(String name) {
        this.name = name;
    }

}
