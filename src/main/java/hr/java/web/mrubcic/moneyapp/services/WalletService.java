package hr.java.web.mrubcic.moneyapp.services;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface WalletService {

    List<Wallet> getUserWallets(final String username);

    List<Wallet> getAll();

    Wallet getById(final Long id);

    @Transactional
    Wallet save(final Wallet wallet);

    @Transactional
    Wallet update(final Wallet wallet);

    @Transactional
    void deleteById(final Long id);

}
