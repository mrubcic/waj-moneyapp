package hr.java.web.mrubcic.moneyapp.services.impl;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.repositories.WalletRepository;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final UserService userService;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository, UserService userService) {
        this.walletRepository = walletRepository;
        this.userService = userService;
    }

    public List<Wallet> getUserWallets(final String username) {
        return (List<Wallet>) walletRepository.findAllByUser_Username(username);
    }


    public List<Wallet> getAll() {
        return (List<Wallet>) walletRepository.findAll();
    }


    public Wallet getById(final Long id) {
        return walletRepository.findById(id).get();
    }


    public Wallet save(final Wallet wallet) {

        final Wallet persistedWallet = walletRepository.save(wallet);
        persistedWallet.setUser(userService.getUser(persistedWallet.getUser().getUsername()));

        return persistedWallet;
    }


    public Wallet update(final Wallet wallet) {
        return walletRepository.save(wallet);
    }


    public void deleteById(final Long id) {
        walletRepository.deleteById(id);
    }

}
