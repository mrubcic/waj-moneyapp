package hr.java.web.mrubcic.moneyapp.repositories;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExpenseRepository extends CrudRepository<Expense, Long> {

    List<Expense> findAllByWallet_Id(final Long id);

    void deleteAllByWallet_Id(final Long id);

    List<Expense> findAllByNameStartingWithIgnoreCase(final String keyword);
}

