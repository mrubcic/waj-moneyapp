package hr.java.web.mrubcic.moneyapp.repositories;

import hr.java.web.mrubcic.moneyapp.models.Wallet;
import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<Wallet, Long> {

    Iterable<Wallet> findAllByUser_Username(final String username);

}
