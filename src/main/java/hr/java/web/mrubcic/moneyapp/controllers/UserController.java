package hr.java.web.mrubcic.moneyapp.controllers;


import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterForm(final Model model){

        model.addAttribute("user", new User());

        return "registerForm";
    }

    @PostMapping("/register")
    public String registerUser(@Validated final User user, final Errors errors, final Model model){

        //check if has errors
        if(errors.hasErrors()){

            return "registerForm";
        }
        //check if user exists
        if(userService.isExistingWithUsername(user.getUsername())){

            model.addAttribute("exists", true);
            return "registerForm";
        }

        //else register user(save)
        userService.createUser(user);
        model.addAttribute("justRegistered", true);

        return "login";
    }
}
