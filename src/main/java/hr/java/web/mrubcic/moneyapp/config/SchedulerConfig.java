package hr.java.web.mrubcic.moneyapp.config;

import hr.java.web.mrubcic.moneyapp.quartz.ExpenseStatisticsJob;
import hr.java.web.mrubcic.moneyapp.quartz.WalletStatisticsJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SchedulerConfig {

    private static final String EXPENSE_JOB_ID = "expenseJob";
    private static final String WALLET_JOB_ID = "walletJob";

    @Bean
    public JobDetail expenseStatsJob(){

        return JobBuilder.newJob(ExpenseStatisticsJob.class)
                .withIdentity(EXPENSE_JOB_ID)
                .storeDurably()
                .build();
    }

    @Bean
    public JobDetail walletStatsJob(){

        return JobBuilder.newJob(WalletStatisticsJob.class)
                .withIdentity(WALLET_JOB_ID)
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger expenseTrigger(){

        final SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(10)
                .repeatForever();

        return TriggerBuilder.newTrigger()
                .forJob(expenseStatsJob())
                .withIdentity(EXPENSE_JOB_ID)
                .withSchedule(scheduleBuilder)
                .build();
    }

    @Bean
    public Trigger walletTrigger(){

        final SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(11)
                .repeatForever();

        return TriggerBuilder.newTrigger()
                .withSchedule(scheduleBuilder)
                .withIdentity(WALLET_JOB_ID)
                .forJob(walletStatsJob())
                .build();
    }




}
