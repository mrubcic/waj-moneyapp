package hr.java.web.mrubcic.moneyapp.quartz;

import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class WalletStatisticsJob extends QuartzJobBean {

    @Autowired
    private WalletService walletService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        final List<Wallet> wallets = walletService.getAll();


        final Map<WalletType, Double> sumByWalletType = wallets.stream()
                .collect(Collectors.groupingBy(Wallet::getWalletType,
                        Collectors.summingDouble(value -> value.walletState().doubleValue())));


        final Map<WalletType, Double> maxByWalletType = wallets.stream()
                .collect(Collectors.toMap(
                        Wallet::getWalletType,
                        value -> value.walletState().doubleValue(),
                        BinaryOperator.maxBy(Double::compareTo)
                ));


        final Map<WalletType, Double> minByWalletType = wallets.stream()
                .collect(Collectors.toMap(
                        Wallet::getWalletType,
                        value -> value.walletState().doubleValue(),
                        BinaryOperator.minBy(Double::compareTo)
                ));

        System.out.println("WALLET STATS: ");
        System.out.println("\t\t SUM\t\t MIN\t\tMAX");
        for (WalletType walletType : WalletType.values()) {

            if(sumByWalletType.get(walletType) != null){

                System.out.println(walletType.name + "\t" +
                        sumByWalletType.get(walletType) + "\t\t" +
                        minByWalletType.get(walletType) + "\t\t" +
                        maxByWalletType.get(walletType) + "\t\t");
            }
        }
        System.out.println("\n\n");
    }
}
