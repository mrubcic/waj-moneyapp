package hr.java.web.mrubcic.moneyapp.controllers.rest;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/expense")
@CrossOrigin
public class ExpenseRestController {

    private final ExpenseService expenseService;

    @Autowired
    public ExpenseRestController(final ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @GetMapping
    public List<Expense> findAll() {
        return expenseService.getAll();
    }

    @GetMapping("/{id}")
    public Expense findOne(@PathVariable final Long id) {
        return expenseService.getById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Expense save(@RequestBody final Expense expense) {
        return expenseService.save(expense);
    }

    @PutMapping("/{id}")
    public Expense update(@RequestBody final Expense expense) {
        return expenseService.update(expense);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        expenseService.deleteById(id);
    }

}
