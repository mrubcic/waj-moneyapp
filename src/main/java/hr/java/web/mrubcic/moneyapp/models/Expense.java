package hr.java.web.mrubcic.moneyapp.models;

import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.models.validation.ExpenseGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Entity
@EqualsAndHashCode(exclude = "wallet")
@Table(name = "EXPENSES")
public final class Expense {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    @NotEmpty(message = "{expense.error.name.required}", groups = ExpenseGroup.class)
    @Size(min = 2, max = 50, message = "{expense.error.name.length}", groups = ExpenseGroup.class)
    private String name;

    @Column(name = "COST")
    @NotNull(message = "{expense.error.cost.required}", groups = ExpenseGroup.class)
    @Positive(message = "{expense.error.cost.negative}", groups = ExpenseGroup.class)
    private BigDecimal cost;

    @Column(name = "COST_TYPE")
    @Enumerated(EnumType.STRING)
    @NotNull(message = "{expense.error.costtype.empty}", groups = ExpenseGroup.class)
    private CostType costType;

    @ManyToOne
//    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "WALLET_ID")
    @Valid
    private Wallet wallet = new Wallet();
}
