package consts;

import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;

import java.math.BigDecimal;
import java.util.*;

public class TestConsts {

    public static final String TEST_USER = "admin";
    public static final String[] TEST_ROLES = {"USER","ADMIN"};
    public static final String TEST_PASS = "adminpass";
    public static final List<Authority> USER_AUTHORITIES = getAuthorities();
    public static final User USER = getUser();
    public static final Wallet WALLET = getWallet();
    public static final List<Wallet> WALLETS = Collections.singletonList(WALLET);
    public static final Map<String, Object> SESSION_ATTRIBUTES = getSessAttributes();
    public static final Expense EXPENSE = getExpense();


    private static final List<Expense> EXPENSES = Collections.singletonList(EXPENSE);

    private static Expense getExpense() {

        final Expense expense = new Expense();

        expense.setCostType(CostType.DRINK);
        expense.setCost(BigDecimal.TEN);
        expense.setName("Pizza");

        return expense;
    }

    private static Map<String, Object> getSessAttributes() {

        final Map<String, Object> attributes = new HashMap<>();
        attributes.put("wallets", WALLETS);
        attributes.put("expenseTypes",CostType.FOOD);
        attributes.put("wallet",WALLET);

        return attributes;
    }



    private static List<Authority> getAuthorities(){

        final List<Authority> authorities  = new ArrayList<>();

        final Authority authorityAdmin = new Authority();
        authorityAdmin.setAuthority("ROLE_ADMIN");
        authorityAdmin.setUsername(TEST_USER);

        final Authority authorityUser = new Authority();
        authorityAdmin.setAuthority("ROLE_USER");
        authorityAdmin.setUsername(TEST_USER);

        authorities.add(authorityAdmin);
        authorities.add(authorityUser);

        return authorities;
    }

    private static User getUser() {

        final User user = new User();
        user.setUsername(TEST_USER);
        user.setPassword(TEST_PASS);
        user.setAuthorities(USER_AUTHORITIES);
        user.setEnabled(true);

        return user;
    }

    private static Wallet getWallet() {

        final Wallet wallet = new Wallet();
        wallet.setUser(USER);
        wallet.setWalletType(WalletType.GUMA);
        wallet.setName("Novcanik");
        wallet.setExpenses(EXPENSES);

        return wallet;
    }
}
