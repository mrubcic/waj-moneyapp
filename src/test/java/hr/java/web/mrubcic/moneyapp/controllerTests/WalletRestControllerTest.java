package hr.java.web.mrubcic.moneyapp.controllerTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import configuration.CustomTestContext;
import consts.TestConsts;
import hr.java.web.mrubcic.moneyapp.controllers.rest.WalletRestController;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static consts.TestConsts.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(WalletRestController.class)
@ContextConfiguration( classes = CustomTestContext.class)
public class WalletRestControllerTest {


    private static final String BASE_URL = "/api/wallet/";
    @Autowired
    MockMvc mockMvc;

    @MockBean
    WalletService walletService;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    public void shouldSaveWallet() throws Exception {

        when(walletService.save(any(Wallet.class))).thenReturn(TestConsts.WALLET);

        final String walletJson = objectMapper.writeValueAsString(WALLET);

        final MvcResult mvcResult = mockMvc.perform(post(BASE_URL)
        .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
        .contentType(MediaType.APPLICATION_JSON)
        .content(walletJson))
                .andExpect(status().isCreated())
                .andReturn();

        final Wallet response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Wallet.class);

        assertEquals(WALLET.getName(), response.getName());
    }

    @Test
    public void shouldReturnList() throws Exception{


        when(walletService.getAll()).thenReturn(WALLETS);

        final MvcResult result = mockMvc.perform(get(BASE_URL)
        .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES)))
                .andExpect(status().isOk())
                .andReturn();


        final List<Wallet> responseList = Arrays.asList(objectMapper.readValue(result.getResponse().getContentAsString(), Wallet[].class));

        assertEquals(1, responseList.size());
    }
}
