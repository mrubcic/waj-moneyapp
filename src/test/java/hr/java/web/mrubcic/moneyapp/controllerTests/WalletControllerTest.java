package hr.java.web.mrubcic.moneyapp.controllerTests;

import configuration.CustomTestContext;
import hr.java.web.mrubcic.moneyapp.controllers.WalletController;
import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(WalletController.class)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CustomTestContext.class)
public class WalletControllerTest {


    private static final String BASE_URL = "/wallet/";
    private static final String TEST_USER = "admin";
    private static final String[] TEST_ROLES = {"USER","ADMIN"};
    private static final String TEST_PASS = "adminpass";
    private static final List<Authority> USER_AUTHORITIES = getAuthorities();
    private static final User USER = getUser();
    private static final Map<String,Object> SESSION_ATTRIBUTES = getSessionAttrs();
    private static final Wallet WALLET = getWallet();

    private static Wallet getWallet() {

        final Wallet wallet = new Wallet();
        wallet.setName("Novcanik");
        wallet.setWalletType(WalletType.GUMA);

        return wallet;
    }

    private static Map<String, Object> getSessionAttrs() {

        final Map<String,Object> atttributes = new HashMap<>();
        atttributes.put("walletTypes", WalletType.values());

        return atttributes;
    }

    private static List<Authority> getAuthorities(){

        final List<Authority> authorities  = new ArrayList<>();

        final Authority authorityAdmin = new Authority();
        authorityAdmin.setAuthority("ROLE_ADMIN");
        authorityAdmin.setUsername(TEST_USER);

        final Authority authorityUser = new Authority();
        authorityAdmin.setAuthority("ROLE_USER");
        authorityAdmin.setUsername(TEST_USER);

        authorities.add(authorityAdmin);
        authorities.add(authorityUser);

        return authorities;
    }


    private static User getUser() {

        final User user = new User();
        user.setUsername(TEST_USER);
        user.setPassword(TEST_PASS);
        user.setAuthorities(USER_AUTHORITIES);
        user.setEnabled(true);

        return user;
    }


    @MockBean
    WalletService walletService;

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;


    @Test
    public void testShowWalletForm() throws Exception {

        mockMvc.perform(get(BASE_URL + "/create/")
        .with(user(TEST_USER).roles(TEST_ROLES).password(TEST_PASS)).with(csrf()))
                .andExpect(model().attributeExists("wallet","walletTypes"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFormCreateWallet() throws Exception {

        //when
        when(walletService.save(any())).thenReturn(WALLET);

        mockMvc.perform(post(BASE_URL + "/create/")
                .with(user(TEST_USER).roles(TEST_ROLES).password(TEST_PASS)).with(csrf())
                .sessionAttrs(SESSION_ATTRIBUTES)
                .param("name",WALLET.getName())
                .param("walletType", WALLET.getWalletType().toString()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expense"));
    }

}
