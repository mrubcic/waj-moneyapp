package hr.java.web.mrubcic.moneyapp.controllerTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import configuration.CustomTestContext;
import hr.java.web.mrubcic.moneyapp.controllers.rest.ExpenseRestController;
import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ExpenseRestController.class)
@ContextConfiguration(classes = CustomTestContext.class)
public class ExpenseRestControllerTest {

    private static final String BASE_URL = "/api/expense/";
    private static final String TEST_USER = "admin";
    private static final String[] TEST_ROLES = {"USER","ADMIN"};
    private static final String TEST_PASS = "adminpass";

    private static final List<Authority> USER_AUTHORITIES = getAuthorities();
    private static final User USER = getUser();
    private static final Wallet WALLET = getWallet();
    private static final List<Wallet> USER_WALLETS = Collections.singletonList(WALLET);
    private static final Expense EXPENSE = getExpense();

    private static final List<Expense> EXPENSES = Collections.singletonList(EXPENSE);
    private static final Long ID = 2L;

    private static Expense getExpense() {

        final Expense expense = new Expense();
        expense.setCostType(CostType.DRINK);
        expense.setCost(BigDecimal.TEN);
        expense.setName("Pizza");
        expense.setId(2L);
        expense.setWallet(WALLET);

        return expense;
    }
    private static List<Authority> getAuthorities(){

        final List<Authority> authorities  = new ArrayList<>();

        final Authority authorityAdmin = new Authority();
        authorityAdmin.setAuthority("ROLE_ADMIN");
        authorityAdmin.setUsername(TEST_USER);

        final Authority authorityUser = new Authority();
        authorityAdmin.setAuthority("ROLE_USER");
        authorityAdmin.setUsername(TEST_USER);

        authorities.add(authorityAdmin);
        authorities.add(authorityUser);

        return authorities;
    }

    private static User getUser() {

        final User user = new User();
        user.setUsername(TEST_USER);
        user.setPassword(TEST_PASS);
        user.setAuthorities(USER_AUTHORITIES);
        user.setEnabled(true);

        return user;
    }

    private static Wallet getWallet() {

        final Wallet wallet = new Wallet();
        wallet.setId(2L);
        wallet.setUser(USER);
        wallet.setWalletType(WalletType.GUMA);
        wallet.setName("Novcanik");
        wallet.setExpenses(EXPENSES);

        return wallet;
    }


    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    ExpenseService expenseService;


    @Test
    public void shouldReturnAll() throws Exception {

        //when
        when(expenseService.getAll()).thenReturn(EXPENSES);

        //test
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL)
        .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES)))
                .andExpect(status().isOk())
                .andReturn();

        final List<Expense> response = Arrays.asList(mapper.readValue(mvcResult.getResponse().getContentAsString(), Expense[].class));

        assertTrue(response.size() > 0);

    }

    @Test
    public void shouldSaveExpense() throws Exception{

        //when
        when(expenseService.save(any(Expense.class))).thenReturn(EXPENSE);

        final String postExpense = mapper.writeValueAsString(EXPENSE);

        final MvcResult mvcResult = mockMvc.perform(post(BASE_URL)
            .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
                .contentType(MediaType.APPLICATION_JSON)
                .content(postExpense))
                .andExpect(status().isCreated())
                .andReturn();

        final Expense response = mapper.readValue(mvcResult.getResponse().getContentAsString(), Expense.class);

        assertEquals(response.getName(), EXPENSE.getName());
    }
}
