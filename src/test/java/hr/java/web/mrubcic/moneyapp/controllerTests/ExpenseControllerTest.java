package hr.java.web.mrubcic.moneyapp.controllerTests;

import configuration.CustomTestContext;
import hr.java.web.mrubcic.moneyapp.controllers.ExpenseController;
import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.models.Wallet;
import hr.java.web.mrubcic.moneyapp.models.enums.CostType;
import hr.java.web.mrubcic.moneyapp.models.enums.WalletType;
import hr.java.web.mrubcic.moneyapp.services.ExpenseService;
import hr.java.web.mrubcic.moneyapp.services.WalletService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ExpenseController.class)
@ContextConfiguration(classes = CustomTestContext.class)
public class ExpenseControllerTest {


    private static final String BASE_URL = "/expense";
    private static final String TEST_USER = "admin";
    private static final String[] TEST_ROLES = {"USER","ADMIN"};
    private static final String TEST_PASS = "adminpass";
    private static final List<Authority> USER_AUTHORITIES = getAuthorities();
    private static final User USER = getUser();
    private static final Wallet WALLET = getWallet();
    private static final List<Wallet> USER_WALLETS = Collections.singletonList(WALLET);
    private static final Map<String, Object> SESSION_ATTRIBUTES = getSessAttributes();
    private static final Expense EXPENSE = getExpense();

    private static final List<Expense> EXPENSES = Collections.singletonList(EXPENSE);

    private static Expense getExpense() {

        final Expense expense = new Expense();
        expense.setWallet(WALLET);
        expense.setCostType(CostType.DRINK);
        expense.setCost(BigDecimal.TEN);
        expense.setName("Pizza");
        expense.setId(2L);

        return expense;
    }

    private static Map<String, Object> getSessAttributes() {

        final Map<String, Object> attributes = new HashMap<>();
        attributes.put("wallets",USER_WALLETS);
        attributes.put("expenseTypes",CostType.FOOD);
        attributes.put("wallet",WALLET);

        return attributes;
    }



    private static List<Authority> getAuthorities(){

        final List<Authority> authorities  = new ArrayList<>();

        final Authority authorityAdmin = new Authority();
        authorityAdmin.setAuthority("ROLE_ADMIN");
        authorityAdmin.setUsername(TEST_USER);

        final Authority authorityUser = new Authority();
        authorityAdmin.setAuthority("ROLE_USER");
        authorityAdmin.setUsername(TEST_USER);

        authorities.add(authorityAdmin);
        authorities.add(authorityUser);

        return authorities;
    }

    private static User getUser() {

        final User user = new User();
        user.setUsername(TEST_USER);
        user.setPassword(TEST_PASS);
        user.setAuthorities(USER_AUTHORITIES);
        user.setEnabled(true);

        return user;
    }

    private static Wallet getWallet() {

        final Wallet wallet = new Wallet();
        wallet.setId(2L);
        wallet.setUser(USER);
        wallet.setWalletType(WalletType.GUMA);
        wallet.setName("Novcanik");
        wallet.setExpenses(EXPENSES);

        return wallet;
    }


    @Autowired
    MockMvc mockMvc;

    @MockBean
    ExpenseService expenseService;

    @MockBean
    WalletService walletService;

    @Test
    public void testShowForm() throws Exception {

        //when
        when(walletService.getUserWallets(TEST_USER)).thenReturn(USER_WALLETS);

        //test
        mockMvc.perform(get(BASE_URL).with(user(TEST_USER)
                .roles(TEST_ROLES)
                .password(TEST_PASS)))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("wallets"))
                .andExpect(view().name("expenseForm"));
    }

    @Test
    public void testFormResult_shouldReturnOk() throws Exception{

        //setup
        WALLET.setExpenses(EXPENSES);
        //when
        when(expenseService.save(any(Expense.class))).thenReturn(EXPENSE);

        //test
        mockMvc.perform(post(BASE_URL)
                .with(user(TEST_USER)
                .roles(TEST_ROLES)
                .password(TEST_PASS))
                .with(csrf())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name",EXPENSE.getName())
                .param("costType",EXPENSE.getCostType().toString())
                .param("cost",EXPENSE.getCost().toPlainString())
                .param("wallet.id",EXPENSE.getWallet().getId().toString())
                .sessionAttrs(SESSION_ATTRIBUTES))
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attributeExists("date","expense","wallet"))
                .andExpect(view().name("formResponse"));
    }

    @Test
    public void testResetWallet() throws Exception {


        mockMvc.perform(get(BASE_URL + "/resetWallet")
                .sessionAttrs(SESSION_ATTRIBUTES)
                .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
                .with(csrf()))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testShowSearchScreen() throws Exception {

        mockMvc.perform(get(BASE_URL + "/search/")
                .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
                .sessionAttrs(SESSION_ATTRIBUTES))
                .andExpect(status().isOk())
                .andExpect(view().name("searchExpenses"))
                .andExpect(model().attributeExists("searchedExpenses","keyword"));
    }

    @Test
    public void testPostSearchExpenses() throws Exception{

        //setup
        final String keyword = "Pizza";

        //when
        when(expenseService.getExpensesStartingWith(keyword)).thenReturn(EXPENSES);


        mockMvc.perform(post(BASE_URL + "/search/")
                .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
                .with(csrf())
                .sessionAttrs(SESSION_ATTRIBUTES)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("keyword",keyword))
                .andExpect(status().isOk())
                .andExpect(view().name("searchExpenses"))
                .andExpect(model().attribute("searchedExpenses", EXPENSES));
    }

}
