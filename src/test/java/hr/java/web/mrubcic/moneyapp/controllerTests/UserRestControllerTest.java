package hr.java.web.mrubcic.moneyapp.controllerTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import configuration.CustomTestContext;
import hr.java.web.mrubcic.moneyapp.controllers.rest.UserRestController;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRestController.class)
@ContextConfiguration(classes = CustomTestContext.class)
public class UserRestControllerTest {


    private final static User USER = getUser();
    private static final String TEST_USER = "admin";
    private static final String[] TEST_ROLES = {"USER","ADMIN"};
    private static final String TEST_PASS = "adminpass";
    private static final String USERNAME = "Username";
    private static final String PASSWORD = "Password";
    private static final String BASE_URL = "/api/user";

    private static User getUser() {

        final User user = new User();
        user.setUsername(USERNAME);
        user.setPassword(PASSWORD);

        return user;
    }

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    UserService userService;


    @Test
    public void shouldSaveUser() throws Exception {

        //when
        when(userService.createUser(any(User.class))).thenReturn(USER);


        //setup
        final String jsonRequest = mapper.writeValueAsString(USER);

        //test
        final MvcResult result = mockMvc.perform(post(BASE_URL)
                .with(user(TEST_USER).password(TEST_PASS).roles(TEST_ROLES))
                .with(csrf())
        .contentType(MediaType.APPLICATION_JSON)
        .content(jsonRequest))
                .andExpect(status().isOk())
                .andReturn();

        final User responseUser = mapper.readValue(result.getResponse().getContentAsString(), User.class);

        assertEquals(USER.getUsername(), responseUser.getUsername());
        assertNull(responseUser.getPassword());
    }

}
