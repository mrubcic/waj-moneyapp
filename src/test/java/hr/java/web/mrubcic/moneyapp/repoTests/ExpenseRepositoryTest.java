package hr.java.web.mrubcic.moneyapp.repoTests;

import hr.java.web.mrubcic.moneyapp.models.Expense;
import hr.java.web.mrubcic.moneyapp.repositories.ExpenseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class ExpenseRepositoryTest {


    private static final int NUM_OF_EXPENSES = 3;
    private static final Long WALLET_ID = 1L;


    @Autowired
    ExpenseRepository expenseRepository;


    @Test
    public void shouldReturnAllExpenses(){

        final List<Expense> expenses = (List<Expense>) expenseRepository.findAll();
        assertEquals(NUM_OF_EXPENSES, expenses.size());
    }

    @Test
    public void shouldFindAllByWalletId(){

        final List<Expense> expenses = expenseRepository.findAllByWallet_Id(WALLET_ID);

        assertFalse(expenses.isEmpty());
    }

    @Test
    public void shouldDeleteWallet(){

        expenseRepository.deleteAllByWallet_Id(WALLET_ID);

        assertTrue(expenseRepository.findAllByWallet_Id(WALLET_ID).isEmpty());

    }

    @Test
    public void shouldfindAllByNameStartingWith(){

        final List<Expense> expenses = expenseRepository.findAllByNameStartingWithIgnoreCase("Pizza");
        assertEquals(1, expenses.size());
    }



}
