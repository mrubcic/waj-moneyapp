package hr.java.web.mrubcic.moneyapp.controllerTests;

import configuration.CustomTestContext;
import hr.java.web.mrubcic.moneyapp.controllers.UserController;
import hr.java.web.mrubcic.moneyapp.models.Authority;
import hr.java.web.mrubcic.moneyapp.models.User;
import hr.java.web.mrubcic.moneyapp.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@ContextConfiguration(classes = CustomTestContext.class)
public class UserControllerTest {

    private static final String BASE_URL = "/user/";
    private static final String TEST_USER = "admin";
    private static final String[] TEST_ROLES = {"USER","ADMIN"};
    private static final String TEST_PASS = "adminpass";
    private static final List<Authority> USER_AUTHORITIES = getAuthorities();
    private static final User USER = getUser();

    private static List<Authority> getAuthorities(){

        final List<Authority> authorities  = new ArrayList<>();

        final Authority authorityAdmin = new Authority();
        authorityAdmin.setAuthority("ROLE_ADMIN");
        authorityAdmin.setUsername(TEST_USER);

        final Authority authorityUser = new Authority();
        authorityAdmin.setAuthority("ROLE_USER");
        authorityAdmin.setUsername(TEST_USER);

        authorities.add(authorityAdmin);
        authorities.add(authorityUser);

        return authorities;
    }


    private static User getUser() {

        final User user = new User();
        user.setUsername(TEST_USER);
        user.setPassword(TEST_PASS);
        user.setAuthorities(USER_AUTHORITIES);
        user.setEnabled(true);

        return user;
    }

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testShowRegisterForm() throws Exception {

        mockMvc.perform(get(BASE_URL + "/register"))
                .andExpect(model().attributeExists("user"))
                .andExpect(status().isOk());
    }

    @Test
    public void testPostUser() throws Exception{

        //when
        when(userService.createUser(any(User.class))).thenReturn(USER);
        when(userService.isExistingWithUsername(USER.getUsername())).thenReturn(false);

        mockMvc.perform(post(BASE_URL + "/register")
                .with(csrf())
                .param("username",USER.getUsername())
                .param("password", USER.getPassword()))
                .andExpect(model().attribute("justRegistered",true))
                .andExpect(view().name("login"));
    }

    @Test
    public void testPostExistingUser() throws Exception{

        //when
        when(userService.createUser(any(User.class))).thenReturn(USER);
        when(userService.isExistingWithUsername(USER.getUsername())).thenReturn(true);

        mockMvc.perform(post(BASE_URL + "/register")
                .with(csrf())
                .param("username",USER.getUsername())
                .param("password", USER.getPassword()))
                .andExpect(model().attribute("exists",true))
                .andExpect(view().name("registerForm"));
    }






}
